package main;

import org.junit.*;
import org.mockito.Mockito;

import client.Main;

/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class TestMain {

	private Main m_testMainMock;
	
	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.m_testMainMock=Mockito.mock(Main.class);
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 
	 */
	/*@Test
	public void testMain() {
		fail("Not yet implemented");
	}*/
	
	/**
	 * 
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testmain() {
		this.m_testMainMock.main(null);
		Mockito.verify(this.m_testMainMock, Mockito.times(1)).main(null);
	}

}
