package view;

import static org.junit.Assert.*;
import java.util.HashMap;
import javax.swing.event.CaretEvent;
import javax.swing.text.BadLocationException;
import org.junit.*;
import org.mockito.Mockito;
import receiver.*;

/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class TestIHMImpl {

	private IHMImpl m_testIHMImpl;
	private Engine m_engineMock;
	private String m_testString;
	
	
	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.m_testString="Test String";
		this.m_engineMock=Mockito.mock(EngineImpl.class);
		this.m_testIHMImpl=new IHMImpl("Smart Editor", this.m_engineMock);
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 
	 */
	/*@Test
	public void testIHMImpl() {
		fail("Not yet implemented");
	}*/

	/**
	 * 
	 */
	@Test
	public void testGetInsert() {
		// Insert test String with event
		this.m_testIHMImpl.getM_InputPanel().setText(this.m_testString);
		
		//Check if insert as been called on the engin mockito with the correct parameter
		Mockito.verify(this.m_engineMock).insert(this.m_testString);
	}

	/**
	 * @throws BadLocationException 
	 * 
	 */
	@Test
	public void testGetRemove() throws BadLocationException {
		// Insert test String with event
		this.m_testIHMImpl.getM_InputPanel().setText(this.m_testString);
		
		// Remove it from InputPanel
		this.m_testIHMImpl.getM_InputPanel().getDocument().remove(0, this.m_testString.length());

		
		// Create the correct parameter for remove
		HashMap<String, Integer> removedSelection=new HashMap<String, Integer>();
		removedSelection.put("start", 0);
		removedSelection.put("end", this.m_testString.length());
		
		
		//Check if the same parameter as been pass to remove
		Mockito.verify(this.m_engineMock).remove(removedSelection);
	}

	/**
	 * 
	 */
	@Test
	public void testGetSelection() {
		// Insert test String with event
		this.m_testIHMImpl.getM_InputPanel().setText(this.m_testString);
		
		// Select all
		this.m_testIHMImpl.getM_InputPanel().selectAll();
		
		// Retreive the selection
		HashMap<String, Integer> gettedSelection=this.m_testIHMImpl.getSelection();
		
		// Put selection into var
		int start=gettedSelection.get("start");
		int end=gettedSelection.get("end");
		
		// Check if the selection as been correctly retrieve
		assertEquals(start, 0);
		assertEquals(end, this.m_testString.length());
	}

	/**
	 * @throws BadLocationException 
	 * 
	 */
	@Test(expected=BadLocationException.class)
	public void testRemoveString() throws BadLocationException {
		// Insert test String with event
		this.m_testIHMImpl.getM_InputPanel().setText(this.m_testString);
		
		// Remove all with removeString method
		this.m_testIHMImpl.removeString(0, this.m_testString.length());
		
		// Select all
		this.m_testIHMImpl.getM_InputPanel().selectAll();
		
		// Retreive the selection
		HashMap<String, Integer> removedSelection=this.m_testIHMImpl.getSelection();
		
		// Put selection into var
		int start=removedSelection.get("start");
		int end=removedSelection.get("end");
		
		// Check if the selection begin offset 0 and end to 0 (all testString removed)
		assertEquals(start, 0);
		assertEquals(end, 0);
		
		// Check Throwing
		this.m_testIHMImpl.removeString(-5, -4);



	}

	/**
	 * 
	 */
	@Test
	public void testGetCursorPosition() {
		// Insert test String with event
		this.m_testIHMImpl.getM_InputPanel().setText(this.m_testString);
		this.m_testIHMImpl.getM_InputPanel().setCaretPosition(this.m_testString.length());
		
		// Check the cursor position
		assertEquals(this.m_testIHMImpl.getCursorPosition(), this.m_testString.length());
	}

	/**
	 * @throws BadLocationException 
	 * 
	 */
	@Test(expected=BadLocationException.class)
	public void testInsertString() throws BadLocationException {
		// Insert test insertString method
		this.m_testIHMImpl.insertString(this.m_testString, 0);
		
		// Check if the string have been insert
		assertTrue(this.m_testIHMImpl.getM_InputPanel().getText().equals(this.m_testString));
		
		// Test Throw
		try {
			this.m_testIHMImpl.insertString(this.m_testString, -1);
		} catch (Exception e) {
			throw(e);
		}
	}

	/**
	 * 
	 */
	@Test
	public void testCaretUpdate() {
		// Fire a caret update
		this.m_testIHMImpl.caretUpdate(Mockito.mock(CaretEvent.class));
		
		// Check if the update as been made
		((EngineImpl) Mockito.verify(this.m_engineMock, Mockito.atLeastOnce())).update(this.m_testIHMImpl);
	}

	/**
	 * 
	 */
	/*@Test
	public void testChangedUpdate() {
		fail("Not yet implemented");
	}*/

	/**
	 * 
	 */
	@Test
	public void testInsertUpdate() {
		// Insert test String with event
		this.m_testIHMImpl.getM_InputPanel().setText(this.m_testString);
		
		// Test if the insert method as been call with the correct parameter
		Mockito.verify(this.m_engineMock).insert(this.m_testString);
	}

	/**
	 * @throws BadLocationException 
	 * 
	 */
	@Test
	public void testRemoveUpdate() throws BadLocationException {
		// Insert test String with event
		this.m_testIHMImpl.getM_InputPanel().setText(this.m_testString);
		
		// Select all
		//this.m_testIHMImpl.getM_InputPanel().selectAll();
		
		// Create the correct parameter for remove
		HashMap<String, Integer> removedSelection=new HashMap<String, Integer>();
		removedSelection.put("start", 0);
		removedSelection.put("end", this.m_testString.length());
		
		// Remove it from InputPanel
		this.m_testIHMImpl.getM_InputPanel().getDocument().remove(0, this.m_testString.length());

		
		//Check if the same parameter as been pass to remove
		Mockito.verify(this.m_engineMock).remove(removedSelection);
	}

	/**
	 * 
	 */
	@Test
	public void testAttach() {
		// Test if engine is attach
		assertTrue(this.m_testIHMImpl.isAttached((IHMObserver) this.m_engineMock));
	}

	/**
	 * 
	 */
	@Test
	public void testDetach() {
		// Detach the engine
		this.m_testIHMImpl.detach((IHMObserver) this.m_engineMock);

		
		// Test if is really attach
		assertFalse(this.m_testIHMImpl.isAttached((IHMObserver) this.m_engineMock));
	}

	/**
	 * 
	 */
	@Test
	public void testIsAttached() {
		// Test if is really attach
		assertTrue(this.m_testIHMImpl.isAttached((IHMObserver) this.m_engineMock));
		
		// Detach the engine
		this.m_testIHMImpl.detach((IHMObserver) this.m_engineMock);

		
		// Test if is really attach
		assertFalse(this.m_testIHMImpl.isAttached((IHMObserver) this.m_engineMock));
	}

	/**
	 * 
	 */
	@Test
	public void testNotifyObservers() {
		this.m_testIHMImpl.notifyObservers();
		((EngineImpl) Mockito.verify(this.m_engineMock, Mockito.atLeastOnce())).update(this.m_testIHMImpl);
	}

}
