package receiver;

import static org.junit.Assert.*;
import org.junit.*;

/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class TestBuffer {

	private Buffer m_testBuffer;
	
	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.m_testBuffer=new Buffer();
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 
	 */
	/*@Test
	public void testBuffer() {
		fail("Not yet implemented");
	}*/

	/**
	 * 
	 */
	@Test
	public void testSetM_content() {
		StringBuffer testString=new StringBuffer();
		testString.insert(0, "Test String");
		this.m_testBuffer.setM_content(testString);
		
		//Test if the content as been set
		assertTrue(this.m_testBuffer.getM_content().toString().equals(testString.toString()));
	}

	/**
	 * 
	 */
	@Test
	public void testGetM_content() {
		StringBuffer testString=new StringBuffer();
		testString.insert(0, "Test String");
		this.m_testBuffer.setM_content(testString);
		
		//Test if the content as been set
		assertTrue(this.m_testBuffer.getM_content().toString().equals(testString.toString()));
	}

}
