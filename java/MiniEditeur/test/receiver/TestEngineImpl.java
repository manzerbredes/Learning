package receiver;

import static org.junit.Assert.*;
import java.util.HashMap;
import org.junit.*;
import org.mockito.Mockito;
import view.*;

/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class TestEngineImpl {

	private EngineImpl m_testEngineImpl;
	private IHMSubject m_ihmMock;
	private HashMap<String, Integer> m_selection;
	private String m_testString;

	
	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Init test string
		this.m_testString="Test String";
		
		// Init SubjectMock
		this.m_ihmMock=Mockito.mock(IHMImpl.class);
		
		// Init current Selection
		this.m_selection=new HashMap<String, Integer>();
		this.m_selection.put("start", 0);
		this.m_selection.put("end", this.m_testString.length());
		
		// Init Mock method
		Mockito.when(this.m_ihmMock.getSelection()).thenReturn(this.m_selection);
		Mockito.when(this.m_ihmMock.getCursorPosition()).thenReturn(0);
		
		// Init Engine
		this.m_testEngineImpl=new EngineImpl();
		
		// Update Engine with SubjectMock
		this.m_testEngineImpl.update(this.m_ihmMock);
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 
	 */
	/*@Test
	public void testEngineImpl() {
		fail("Not yet implemented");
	}*/

	/**
	 * 
	 */
	@Test
	public void testCopy() {
		// Insert text
		this.m_testEngineImpl.insert(this.m_testString);
		
		// Copy it to clipboard
		this.m_testEngineImpl.copy();
		
		// Check if text as been realy copied to clipboard
		assertTrue(this.m_testEngineImpl.getClipboardContent().toString().equals(this.m_testString));
	}

	/**
	 * 
	 */
	@Test
	public void testCut() {
		// Insert text
		this.m_testEngineImpl.insert(this.m_testString);
		
		// Cut it to clipboard
		this.m_testEngineImpl.cut();
		
		// Check if text as been realy copied to clipboard
		assertTrue(this.m_testEngineImpl.getClipboardContent().toString().equals(this.m_testString));
		
		// Test if text as been removed from buffer
		assertTrue(this.m_testEngineImpl.getM_BufferContent().equals(""));
	}

	/**
	 * 
	 */
	@Test
	public void testPaste() {
		// Insert text
		this.m_testEngineImpl.insert(this.m_testString);
		
		// Cut it to clipboard
		this.m_testEngineImpl.cut();
		
		// Test if text as been removed from buffer
		assertTrue(this.m_testEngineImpl.getM_BufferContent().equals(""));
		
		// Apply the paste
		this.m_testEngineImpl.paste();
		
		// Test if text as been removed from buffer
		assertTrue(this.m_testEngineImpl.getM_BufferContent().equals(this.m_testString));
	}

	/**
	 * 
	 */
	@Test
	public void testInsert() {
		// Test if text as been removed from buffer
		assertTrue(this.m_testEngineImpl.getM_BufferContent().equals(""));
		
		// Insert text
		this.m_testEngineImpl.insert(this.m_testString);
		
		// Test if text as been removed from buffer
		assertTrue(this.m_testEngineImpl.getM_BufferContent().equals(this.m_testString));
	}

	/**
	 * 
	 */
	@Test
	public void testRemove() {
		// Insert text
		this.m_testEngineImpl.insert(this.m_testString);
		
		// Test if text as been removed from buffer
		assertTrue(this.m_testEngineImpl.getM_BufferContent().equals(this.m_testString));
		
		// Remove
		this.m_testEngineImpl.remove(this.m_selection);
		
		// Test if text as been removed from buffer
		assertTrue(this.m_testEngineImpl.getM_BufferContent().equals(""));
		
	}

	/**
	 * 
	 */
	@Test
	public void testUpdate() {
		// Set a new random cursor pos
		int cursorPosTest=251;
		
		//Select all witouth last char
		int selectEnd=this.m_testString.length()-1;
		
		// Change Selection
		this.m_selection=new HashMap<String, Integer>();
		this.m_selection.put("start", 0);
		this.m_selection.put("end", selectEnd);
		this.m_testEngineImpl.insert(this.m_testString);
		
		// Adapt Mock method
		Mockito.when(this.m_ihmMock.getSelection()).thenReturn(this.m_selection);
		Mockito.when(this.m_ihmMock.getCursorPosition()).thenReturn(cursorPosTest);
		this.m_testEngineImpl.update(this.m_ihmMock);
		
		// Check if cursor position as been updated
		assertEquals("Error, cursor position not updated", cursorPosTest, this.m_testEngineImpl.getCursorPosition());
		
		// Cut with a new Selection (0 to selectEnd)
		this.m_testEngineImpl.cut();
		
		// Check if the last char of m_testString is intact
		assertTrue(this.m_testEngineImpl.getM_BufferContent().equals(this.m_testString.charAt(selectEnd)+""));
	}

	/**
	 * 
	 */
	@Test
	public void testGetClipboardContent() {
		// Insert text
		this.m_testEngineImpl.insert(this.m_testString);
		
		// Copy it to clipboard
		this.m_testEngineImpl.copy();
		
		// Check clipboard content
		assertTrue(this.m_testString.equals(this.m_testEngineImpl.getClipboardContent()));
	}

	/**
	 * 
	 */
	@Test
	public void testGetCursorPosition() {
		// Set a new random cursor pos
		int cursorPosTest=251;
		
		//Select all witouth last char
		int selectEnd=this.m_testString.length()-1;
		
		// Change Selection
		this.m_selection=new HashMap<String, Integer>();
		this.m_selection.put("start", 0);
		this.m_selection.put("end", selectEnd);
		this.m_testEngineImpl.insert(this.m_testString);
		
		// Adapt Mock method
		Mockito.when(this.m_ihmMock.getSelection()).thenReturn(this.m_selection);
		Mockito.when(this.m_ihmMock.getCursorPosition()).thenReturn(cursorPosTest);
		this.m_testEngineImpl.update(this.m_ihmMock);
		
		// Check  for getCursorPosition method
		assertEquals("Error, fail to get cursor position", cursorPosTest, this.m_testEngineImpl.getCursorPosition());
				
	}
}
