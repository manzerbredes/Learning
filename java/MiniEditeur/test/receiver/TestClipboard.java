package receiver;

import static org.junit.Assert.*;
import org.junit.*;

/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class TestClipboard {

	private Clipboard m_testClipboard;
	
	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.m_testClipboard=new Clipboard();
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 
	 */
	/*@Test
	public void testClipboard() {
		fail("Not yet implemented");
	}*/

	/**
	 * 
	 */
	@Test
	public void testSetM_content() {
		String testString="Test String";
		this.m_testClipboard.setM_content(testString);
		
		//Test if the content as been set
		assertTrue(this.m_testClipboard.getM_content().toString().equals(testString.toString()));
	}

	/**
	 * 
	 */
	@Test
	public void testGetM_content() {
		String testString="Test String";
		this.m_testClipboard.setM_content(testString);
		
		//Test if the content as been get
		assertTrue(this.m_testClipboard.getM_content().toString().equals(testString.toString()));;
	}

}
