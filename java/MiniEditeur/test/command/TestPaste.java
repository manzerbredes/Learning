package command;


import java.util.HashMap;
import javax.swing.text.BadLocationException;
import org.junit.*;
import org.mockito.*;
import receiver.*;
import view.*;


/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class TestPaste {

	private Paste m_testPaste;
	private Engine m_engineMock;
	private IHM m_ihmMock;
	
	private HashMap<String, Integer> m_selection;
	
	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Create Mockito
		this.m_engineMock=Mockito.mock(EngineImpl.class);
		this.m_ihmMock=Mockito.mock(IHMImpl.class);
		this.m_selection=new HashMap<String, Integer>();
		m_selection.put("start", 0);
		m_selection.put("end", 0);

		Mockito.when(this.m_ihmMock.getSelection()).thenReturn(m_selection);
		
		// Create copy object
		this.m_testPaste=new command.Paste(this.m_engineMock, this.m_ihmMock);
	
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/*@Test
	public void testCopy() {
		fail("Not yet implemented");
	}*/

	/**
	 * @throws BadLocationException 
	 * 
	 */
	@Test
	public void testExecute() throws BadLocationException {
		this.m_testPaste.execute();
		Mockito.verify(this.m_engineMock, Mockito.times(1)).paste();
		Mockito.verify(this.m_ihmMock, Mockito.times(1)).getSelection();
		
		// Test when start < end
		this.m_selection.clear();
		this.m_selection.put("start", 0);
		this.m_selection.put("end", 1);


		this.m_testPaste.execute();

		Mockito.verify(this.m_engineMock, Mockito.times(2)).paste();
		Mockito.verify(this.m_ihmMock, Mockito.times(2)).getSelection();
		// One more method will be execute
		Mockito.verify(this.m_ihmMock, Mockito.times(1)).removeString(this.m_selection.get("start"),this.m_selection.get("end"));

	}

}
