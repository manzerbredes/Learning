package command;

import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.*;
import receiver.Engine;
import receiver.EngineImpl;
import view.IHM;
import view.IHMImpl;


/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class TestRemove {

	private Remove m_testRemove;
	private Engine m_engineMock;
	private IHM m_ihmMock;
	
	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Create Mockito
		this.m_engineMock=Mockito.mock(EngineImpl.class);
		this.m_ihmMock=Mockito.mock(IHMImpl.class);
		HashMap<String, Integer> selection=new HashMap<String, Integer>();
		selection.put("start", 0);
		selection.put("end", 0);

		Mockito.when(this.m_ihmMock.getRemove()).thenReturn(selection);
		

		// Create copy object
		this.m_testRemove=new Remove(this.m_engineMock, this.m_ihmMock);
	
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/*@Test
	public void testCopy() {
		fail("Not yet implemented");
	}*/

	/**
	 * 
	 */
	@Test
	public void testExecute() {
		this.m_testRemove.execute();
		Mockito.verify(this.m_ihmMock, Mockito.times(1)).getRemove();
		Mockito.verify(this.m_engineMock, Mockito.times(1)).remove(this.m_ihmMock.getRemove());
	}

}
