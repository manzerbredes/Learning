package command;

import org.junit.*;
import org.mockito.*;
import receiver.*;
import view.*;

/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class TestInsert {

	private Insert m_testInsert;
	private Engine m_engineMock;
	private IHM m_ihmMock;
	
	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Create Mockito
		this.m_engineMock=Mockito.mock(EngineImpl.class);
		this.m_ihmMock=Mockito.mock(IHMImpl.class);
		Mockito.when(this.m_ihmMock.getInsert()).thenReturn("Test String");

		// Create copy object
		this.m_testInsert=new Insert(this.m_engineMock, this.m_ihmMock);
	
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/*@Test
	public void testCopy() {
		fail("Not yet implemented");
	}*/

	/**
	 * 
	 */
	@Test
	public void testExecute() {
		this.m_testInsert.execute();
		Mockito.verify(this.m_ihmMock, Mockito.times(1)).getInsert();
		Mockito.verify(this.m_engineMock, Mockito.times(1)).insert(this.m_ihmMock.getInsert());
	}

}
