package view;

import java.util.HashMap;

import javax.swing.text.BadLocationException;

/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * IHM interface
 */
public interface IHM {
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return String
	 * 
	 * Retrieve last string insert in IHM
	 */
	public String getInsert();
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return HashMap<String,Integer>
	 * 
	 * Retrieve string range who have been remove in IHM
	 */
	public HashMap<String,Integer> getRemove();
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param content contain the String to insert
	 * @param position contain the position where you will insert the String
	 * 
	 * Insert a new string in IHM at position
	 * @throws BadLocationException 
	 */
	public void insertString(String content, int position) throws BadLocationException;
	
	

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 *
	 * @param start
	 * @param end
	 * 
	 * Remove string from the IHM
	 * @throws BadLocationException 
	 * 
	 */
	public void removeString(int start, int end) throws BadLocationException;
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return HashMap<String,Integer>
	 * 
	 * Get the IHM selection
	 * 
	 */
	public HashMap<String,Integer> getSelection();

}
