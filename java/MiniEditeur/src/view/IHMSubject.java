package view;

import java.util.HashMap;

/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * IHMSubject Interface
 */
public interface IHMSubject {

	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param observer contain the IHMObserver to attach to the subject
	 * 
	 * Attach an observer
	 */
	public void attach(IHMObserver observer);
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param observer contain the IHMObserver to detach to the subject
	 * 
	 * Detach an observer
	 */
	public void detach(IHMObserver observer);
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param observer contain the IHMObserver to test if it's attached
	 * @return boolean true if observer is attach and false else
	 * 
	 * Update the IHMObserver with subject
	 */
	public boolean isAttached(IHMObserver observer);

	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Method to notify a Observer
	 */
	public void notifyObservers();
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return int contain cursor position
	 * 
	 * Get the cursor position
	 */
	public int getCursorPosition();
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return HashMap<String,Integer> contain the current selection
	 * 
	 * Get the current selection in a HashMap
	 */
	public HashMap<String,Integer> getSelection();

	
}
