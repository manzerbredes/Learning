package view;

import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import receiver.Engine;
import command.*;


/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * IHMImpl Class
 */
public class IHMImpl extends JFrame implements IHM,IHMSubject, CaretListener, DocumentListener{

	
	/**
	 * Declare default serialVersionUID number
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Contain the observer
	 */
	private ArrayList<IHMObserver> m_IHMObservers;
	
	/**
	 * Contain all insertion in a LIFO stack object
	 */
	private LinkedList<String> m_insertQueue;
	
	
	/**
	 * Contain all removed text in a LIFO stack object
	 */
	private LinkedList<HashMap<String,Integer>> m_removeQueue;

	
	/**
	 * Contain pair "command name" -> concrete command object, a simple list to reduce
	 * number of attribute of the class
	 */
	private Map<String,Command> m_commands;

	
	/**
	 * Contain the editor input zone
	 */
	private JEditorPane m_inputPanel;
	
	
	/**
	 * Contain the menu of the main window
	 */
	private MainMenuBar m_mainMenuBar;
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param title contain the String for the title
	 * @param engine contain the engine to link to the commands and attach to the observer
	 * 
	 * Instanciate a new MainWindow
	 */
	public IHMImpl(String title, Engine engine){
		
		
		// Init Engine and observer
		this.m_IHMObservers=new ArrayList<IHMObserver>();
		this.attach((IHMObserver) engine);
		
		// Init insertQueue
		this.m_insertQueue=new LinkedList<String>();
		this.m_removeQueue=new LinkedList<HashMap<String,Integer>>();
	
		// Init all commands
		this.initCommands(engine);
		
		// Configure window
		this.setTitle(title);
		this.setSize(600,600);
		
		// Add menu
		this.m_mainMenuBar=new MainMenuBar();
		this.setJMenuBar(this.m_mainMenuBar);
		
		// Add input panel
		this.m_inputPanel=new JEditorPane();
		this.m_inputPanel.getDocument().addDocumentListener(this);		
		this.add(this.m_inputPanel);
		this.m_inputPanel.addCaretListener(this);
		
		// Define all keybinding action (Cut, Copy, Paste)
		Action cutAction = new AbstractAction("Cut") {

		    /**
			 * Declare default serialVersionUID number
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    public void actionPerformed(ActionEvent e) {
		        m_commands.get("cut").execute();
		        try {
					removeString(m_inputPanel.getSelectionStart(), m_inputPanel.getSelectionEnd());
				} catch (BadLocationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        notifyObservers(); //Notify the new caretPosition after the remove		        
		    }
		};
		Action copyAction = new AbstractAction("Copy") {

		    /**
			 * Declare default serialVersionUID number
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    public void actionPerformed(ActionEvent e) {
		        m_commands.get("copy").execute();   
		    }
		};
		Action pasteAction = new AbstractAction("Paste") {
		    /**
			 * Declare default serialVersionUID number
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    public void actionPerformed(ActionEvent e) {
		        m_commands.get("paste").execute();
		    }
		};
		
		// Apply custom action
		this.m_inputPanel.getKeymap().addActionForKeyStroke(KeyStroke.getKeyStroke("control X"), cutAction);
		this.m_inputPanel.getKeymap().addActionForKeyStroke(KeyStroke.getKeyStroke("control C"), copyAction);
		this.m_inputPanel.getKeymap().addActionForKeyStroke(KeyStroke.getKeyStroke("control V"), pasteAction);

		// Define action when we click on windows close
		WindowListener actionOnExit = new WindowAdapter() {
			
			public void windowClosing(WindowEvent e){
				System.exit(0);
			}
		};

		this.addWindowListener(actionOnExit);
		
		// Display the window
		this.setVisible(true);
	}
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param Engine to link it to all commands
	 * 
	 * Init all commands mapping
	 */
	private void initCommands(Engine engine){
		// Init command HashMap
		this.m_commands=new HashMap<String, Command>();
		
		// Init all commands
		this.m_commands.put("copy", new Copy(engine,this));
		this.m_commands.put("cut", new Cut(engine,this));
		this.m_commands.put("paste", new Paste(engine,this));
		this.m_commands.put("insert", new Insert(engine,this));
		this.m_commands.put("remove", new Remove(engine,this));

	}

	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return String contain the text to insert
	 * 
	 * Init all commands mapping
	 */
	@Override
	public String getInsert() {
		return this.m_insertQueue.poll();
	}

	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return HashMap<String,Integer>
	 * 
	 * Retrieve string range who have been remove in IHM
	 */
	@Override
	public HashMap<String, Integer> getRemove() {
		return this.m_removeQueue.poll();
	}
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return HashMap<String,Integer> contain the current selection
	 * 
	 * Update the IHMObserver with subject
	 */
	@Override
	public HashMap<String,Integer> getSelection() {
		//Create ouput HashMap
		HashMap<String,Integer> selection= new HashMap<String,Integer>();
		
		//Get end selection
		int endSelection=this.m_inputPanel.getSelectionEnd();
		int startSelection=this.m_inputPanel.getSelectionStart();
		//Check if the selection have been made with mouse from left to right (negative number)
		if(startSelection>endSelection){
			int tmp=endSelection;
			endSelection=startSelection;
			startSelection=tmp;
		}
		
		//Put Start Selection
		selection.put("start", startSelection);
		//Put End selection
		selection.put("end", endSelection);

		return selection;
	}

	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Remove a substring from m_inputPanel without run any notify command (CaretEvent and DocumentEvent)
	 * @throws BadLocationException 
	 */
	@Override
	public void removeString(int start, int end) throws BadLocationException {
		// Disable Document listener to avoid remove event when cut
        m_inputPanel.getDocument().removeDocumentListener(this);
        m_inputPanel.removeCaretListener(this);
        
        // Remove content (without run public void removeUpdate(DocumentEvent arg0) from DocumentListener)
        try {
			this.m_inputPanel.getDocument().remove(start, (end-start));
		} catch (BadLocationException e) {
			throw(e);
		}
        
        // Restore Document Listener after cut
        m_inputPanel.getDocument().addDocumentListener(this);
        m_inputPanel.addCaretListener(this);
        
        // Notify to the Observer the update
        this.notifyObservers();
	}
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return int contain cursor position
	 * 
	 * Get the cursor position
	 */
	@Override
	public int getCursorPosition() {
		//Return the start selection (corresponding to the SelectionStart value)
		return this.m_inputPanel.getSelectionStart();
	}
	
	

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Insert a string in the m_inputPanel without run any notify command (CaretEvent and DocumentEvent)
	 */
	@Override
	public void insertString(String content, int position) throws BadLocationException {
		// Disable Document listener to avoid remove event when cut
        m_inputPanel.getDocument().removeDocumentListener(this);
        m_inputPanel.removeCaretListener(this);

        
        // Insert content (without run public void insertUpdate(DocumentEvent arg0) from DocumentListener)
		try {
			this.m_inputPanel.getDocument().insertString(position, content, new SimpleAttributeSet());
		} catch (BadLocationException e) {
			throw(e);
		}
        
        // Restore Document Listener after cut
        m_inputPanel.getDocument().addDocumentListener(this);
        m_inputPanel.addCaretListener(this);
        
        // Notify to the Observer the update
        this.notifyObservers();
	}

	/**
	 * See doc from API
	 */
	@Override
	public void caretUpdate(CaretEvent arg0) {
		this.notifyObservers();
	}
	
	/**
	 * See doc from API
	 */
	@Override
	public void changedUpdate(DocumentEvent arg0) {};

	/**
	 * See doc from API
	 */
	@Override
	public void insertUpdate(DocumentEvent arg0) {

		try {
			this.m_insertQueue.add(m_inputPanel.getText(arg0.getOffset(), arg0.getLength()));
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
		this.m_commands.get("insert").execute();

		
		
	}

	/**
	 * See doc from API
	 */
	@Override
	public void removeUpdate(DocumentEvent arg0) {
		
		HashMap<String, Integer> removedElement=new HashMap<String, Integer>();
		removedElement.put("start", arg0.getOffset());
		removedElement.put("end", arg0.getOffset()+arg0.getLength());
		this.m_removeQueue.add(removedElement);
	
        this.m_commands.get("remove").execute();
			
	}

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param observer contain the IHMObserver to attach to the subject
	 * 
	 * Attach an observer
	 */
	@Override
	public void attach(IHMObserver observer) {
		this.m_IHMObservers.add(observer);
	}

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param observer contain the IHMObserver to detach to the subject
	 * 
	 * Detach an observer
	 */
	@Override
	public void detach(IHMObserver observer) {
		this.m_IHMObservers.remove(observer);
	}

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param observer contain the IHMObserver to test if it's attached
	 * 
	 * Update the IHMObserver with subject
	 */
	@Override
	public boolean isAttached(IHMObserver observer) {
		return this.m_IHMObservers.contains(observer);
	}

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param observer contain the IHMObserver to notify
	 * 
	 * Method to notify a Observer
	 */
	@Override
	public void notifyObservers() {
		Iterator<IHMObserver> i=this.m_IHMObservers.iterator();
		while(i.hasNext()){
			i.next().update(this);
		}
	}
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return JEditorPane the panel of the IHM
	 * 
	 * Method to get the input panel (for unit test)
	 */
	public JEditorPane getM_InputPanel() {
		return this.m_inputPanel;
	}


	
	
	/**
	 * 
	 * @author Loic GUEGAN, Othmane KABIR
	 *
	 * InnerClass to manage menu bar
	 */
	class MainMenuBar extends JMenuBar{

		
		/**
		 * Declare default serialVersionUID number
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Add this menu item as attribute to change
		 * his behavior during execution
		 */
		private JMenuItem macroRecordItem;
		
		/**
		 * Macro is recording or not
		 */
		private boolean macroRecordingState;
		
		
		/**
		 * @author Loic GUEGAN, Othmane KABIR
		 * 
		 * Instanciate a new MainMenuBar
		 */
		public MainMenuBar(){
			
			//Init attributes
			this.macroRecordItem=new JMenuItem();
			this.macroRecordingState=false; //Macros recording disable at startup
			Action toggleMacrosRecording = new AbstractAction("toggleMacrosRecording") {

			    /**
				 * Declare default serialVersionUID number
				 */
				private static final long serialVersionUID = 1L;

				@Override
			    public void actionPerformed(ActionEvent e) {
			        if(macroRecordingState){
			        	macroRecordItem.setText("Stop Recording");
			        }
			        else{
			        	macroRecordItem.setText("Start Recording");
			        }
					macroRecordingState=!(macroRecordingState);

			    }

			};
			this.macroRecordItem.addActionListener(toggleMacrosRecording);
			toggleMacrosRecording.actionPerformed(new ActionEvent(this, 0, "dummy")); //macroRecordItem text
			
			
			//Init menu
			this.add(this.buildFileMenu());
			this.add(this.buildMacroMenu());
			this.add(this.buildHelpMenu());
		}
		
		/**
		 * @author Loic GUEGAN, Othmane KABIR
		 * return JMenu containe the File menu
		 * 
		 * Instanciate a File Menu
		 */
		private JMenu buildFileMenu(){
			//Create the menu
			JMenu FileMenu=new JMenu("File");
			
			//Add Open File item
			FileMenu.add(new JMenuItem("Open"));
			FileMenu.addSeparator();
			
			//Define Action for Exit
			Action exitAction = new AbstractAction("Exit") {

			    /**
				 * Declare default serialVersionUID number
				 */
				private static final long serialVersionUID = 1L;

				@Override
			    public void actionPerformed(ActionEvent e) {
			        System.exit(0); 
			    }

			};
			
			//Add Exit item
			JMenuItem exitMenuItem=new JMenuItem("Exit");
			exitMenuItem.addActionListener(exitAction);
			FileMenu.add(exitMenuItem);

			//Return the FileMenu
			return FileMenu;
		}
		
		
		/**
		 * @author Loic GUEGAN, Othmane KABIR
		 * return JMenu containe the Macros menu
		 * 
		 * Instanciate a Macros Menu
		 */
		private JMenu buildMacroMenu(){
			//Create the menu
			JMenu MacroMenu=new JMenu("Macro");
			
			//Add the recording item
			MacroMenu.add(this.macroRecordItem);

			//Return the MacroMenu
			return MacroMenu;
		}
		
		/**
		 * @author Loic GUEGAN, Othmane KABIR
		 * return JMenu containe the Help menu
		 * 
		 * Instanciate a Help Menu
		 */
		private JMenu buildHelpMenu(){
			//Create the menu
			JMenu HelpMenu=new JMenu("Help");
			
			//Add some item
			HelpMenu.add(new JMenuItem("Commands"));
			HelpMenu.addSeparator();
			HelpMenu.add(new JMenuItem("About"));

			//Return the HelpMenu
			return HelpMenu;
		}

	}


}
