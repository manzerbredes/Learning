package view;

/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * IHMObserver Interface
 */
public interface IHMObserver {
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param subject contain the updated subject
	 * 
	 * Update the IHMObserver with subject
	 */
	public void update(IHMSubject subject);

}
