package command;

import java.util.HashMap;

import javax.swing.text.BadLocationException;

import receiver.Engine;
import view.IHM;

/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * Paste command implementation
 */
public class Paste implements Command {

	/**
	 * Declare engine command
	 */
	private Engine m_engine;
	
	/**
	 * Declare IHM
	 */
	private IHM m_ihm;
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param engine The Engine to link
	 * @param ihm The IHM to link
	 * 
	 * Instanciate a new Paste command
	 */
	public Paste(Engine engine, IHM ihm){
		this.m_engine=engine;
		this.m_ihm=ihm;
	}
	
	/**
	 * (non-Javadoc)
	 * @see command.Command#execute()
	 */
	@Override
	public void execute() {
		this.m_engine.paste();
		
		// Get the IHM selection (because it is deferent from Engine after the paste)
		HashMap<String,Integer> selectionIHM=this.m_ihm.getSelection();
	
		// Check if a text is selected
		if((selectionIHM.get("end")-selectionIHM.get("start")) > 0){
			// If yes remove it
			try {
				this.m_ihm.removeString(selectionIHM.get("start"), selectionIHM.get("end"));
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}
		
		// Insert the new text
		try {
			this.m_ihm.insertString(this.m_engine.getClipboardContent(), this.m_engine.getCursorPosition());
		} catch (BadLocationException e) {
			e.printStackTrace();
		}

	}
}
