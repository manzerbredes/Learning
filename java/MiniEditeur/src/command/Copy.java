package command;

import receiver.Engine;
import view.IHM;

/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * Copy command implementation
 */
public class Copy implements Command {

	/**
	 * Declare engine command
	 */
	private Engine m_engine;
	
	/**
	 * Declare IHM
	 */
	private IHM m_ihm;
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param engine The Engine to link
	 * @param ihm The IHM to link
	 * 
	 * Instanciate a new Copy command
	 */
	public Copy(Engine engine, IHM ihm){
		this.m_engine=engine;
		this.m_ihm=ihm;
	}
	
	/**
	 * (non-Javadoc)
	 * @see command.Command#execute()
	 */
	@Override
	public void execute() {
		this.m_engine.copy();
	}
}
