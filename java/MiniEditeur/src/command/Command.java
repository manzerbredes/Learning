package command;


/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * Command interface
 */
public interface Command {

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Override this method to execute the correct engine method
	 */
	public void execute();
	
}
