package receiver;

import java.util.HashMap;


/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * Engine Interface
 */
public interface Engine {

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Copy the selection from IHM to clipboard
	 */
	public void copy();

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Move the selection from IHM to clipboard
	 */
	public void cut();

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Paste the selection to buffer and IHM
	 */
	public void paste();

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param text contain the String to insert in the Buffer
	 * 
	 * Insert a string to buffer
	 */
	public void insert(String text);

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param removeRange contain the range of the substring of the buffer to remove
	 * 
	 * Remove a substring from buffer
	 */
	public void remove(HashMap<String, Integer> removeRange);
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return String who contain the content of the clipboard
	 * 
	 * Get the content of the clipboard
	 */
	public String getClipboardContent();

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return int contain the cursor position
	 * 
	 * Get the current cursor position
	 */
	public int getCursorPosition();

}
