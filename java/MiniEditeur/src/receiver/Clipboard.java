package receiver;


/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * Clipboard Class
 */
public class Clipboard{

	
	/**
	 * The content of the clipboard
	 */
	private String m_content;
  
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Instanciate a new Clipboard with an empty content
	 */
	public Clipboard(){
		this.m_content=new String();
	}
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param newContent contain the new content to put in the clipboard
	 * 
	 * Set the current content to newContent (a copy)
	 */
	public void setM_content(String newContent){
		this.m_content=newContent;
	}
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return String
	 * 
	 * Get the content of the clipboard (return a copy)
	 */
	public String getM_content(){
		return this.m_content;
	}
}
