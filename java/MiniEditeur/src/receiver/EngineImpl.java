package receiver;

import java.util.HashMap;
import view.IHMObserver;
import view.IHMSubject;

/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * EngineImpl Class
 */
public class EngineImpl implements Engine, IHMObserver {


	/**
	 * Clipboard associated to the Engine
	 */
	private Clipboard m_clipboard;
	
	
	/**
	 *  Buffer associate to the Engine
	 */
	private Buffer m_buffer;
	
	/**
	 * Current cursor position
	 */
	private int m_cursorPosition;
	
	/**
	 * Current selection
	 */
	private int m_startSelection;
	private int m_endSelection;

	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param IHM
	 * 
	 * Instanciate a new EngineImpl
	 */
	public EngineImpl() {
		this.m_buffer=new Buffer();
		this.m_clipboard=new Clipboard();
	};
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * See doc in the interface
	 */
	@Override
	public void copy(){
		// Get Buffer content
		StringBuffer content=this.m_buffer.getM_content();
		
		// Put the selected String to Clipboard
		this.m_clipboard.setM_content(content.substring(this.m_startSelection, this.m_endSelection));
	}

	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * See doc in the interface
	 */
	@Override
	public void cut(){
		// Get Buffer content
		StringBuffer content=this.m_buffer.getM_content();

		// Put the selected String to Clipboard
		this.m_clipboard.setM_content(content.substring(this.m_startSelection,this.m_endSelection));

		// Remove selected text from buffer
		content.delete(this.m_startSelection, this.m_endSelection);
	
		//Update Buffer
		this.m_buffer.setM_content(content);	
	}
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * See doc in the interface
	 */
	@Override
	public void paste(){
		// Get buffer content
		StringBuffer content=this.m_buffer.getM_content();
		
		// Check if a text is selected
		if((m_endSelection-m_startSelection) > 0){
			// Remove the current selection before insert
			content.delete(m_startSelection, m_endSelection);
		}
		
		// Insert the clipboard at the cursor position
		content.insert(this.m_cursorPosition, this.m_clipboard.getM_content());
		
		// Update the buffer content
		this.m_buffer.setM_content(content);
	}
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param String contain the text to insert
	 * 
	 * See doc in the interface
	 */
	@Override
	public void insert(String text){
		// Get Buffer Content
		StringBuffer content=this.m_buffer.getM_content();
		
		// Insert the correct string at the cursor position
		content.insert(this.m_cursorPosition, text);
		
		// Update buffer content
		this.m_buffer.setM_content(content);		
	}

	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param removeRange contain the start index and the end index part to remove from buffer
	 * 
	 * See doc in the interface
	 */
	@Override
	public void remove(HashMap<String, Integer> removeRange){
		// Get Buffer Content
		StringBuffer content=this.m_buffer.getM_content();
		
		// Remove the correct substring from buffer
		content.delete(removeRange.get("start"), removeRange.get("end"));
		
		// Update buffer content
		this.m_buffer.setM_content(content);
	}

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param IHMSubject contain the current subject in observation
	 * 
	 * Update required data
	 */
	@Override
	public void update(IHMSubject subject) {
		//Update all informations
		this.m_cursorPosition=subject.getCursorPosition();
		this.m_startSelection=subject.getSelection().get("start");
		this.m_endSelection=subject.getSelection().get("end");	
	}


	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return String who contain the content of the clipboard
	 * 
	 * Get the content of the clipboard
	 */
	@Override
	public String getClipboardContent() {
		return this.m_clipboard.getM_content();
	}

	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return int contain the cursor position
	 * 
	 * Get the current cursor position
	 */
	@Override
	public int getCursorPosition() {
		return this.m_cursorPosition;
	}
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return String contain the buffer content
	 * 
	 * Get buffer content
	 */
	public String getM_BufferContent() {
		return this.m_buffer.getM_content().toString();
	}
}

