package receiver;


/**
 * @author Loic GUEGAN, Othmane KABIR
 * 
 * Buffer Class
 */
public class Buffer{

	/**
	 * The content of the buffer
	 */
	private StringBuffer m_content;
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * 
	 * Instanciate a new buffer with an empty content
	 */
	public Buffer(){
		this.m_content=new StringBuffer();
	}

	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @param newContent contain the newContent to put in the buffer
	 * 
	 * Set the current content to newContent (a copy of a new content)
	 * 
	 */
	public void setM_content(StringBuffer newContent){
		StringBuffer contentClone=new StringBuffer();
		contentClone.insert(0,newContent.toString());
		this.m_content=contentClone;
	}
	
	
	/**
	 * @author Loic GUEGAN, Othmane KABIR
	 * @return StringBuffer
	 * 
	 * Get a copy of the content of the buffer
	 */
	public StringBuffer getM_content(){
		StringBuffer contentClone=new StringBuffer();
		contentClone.insert(0,this.m_content.toString());
		return contentClone;
	}
	
	

}
