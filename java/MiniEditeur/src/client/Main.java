package client;

import receiver.EngineImpl;
import view.*;


/**
 * @author Loic GUEGAN, Othmane Kabir
 *
 */
public class Main {
	
	
	/**
	 * @param argv The arguments of the program
	 */
	public static void main(String[] argv){
		// Build main window
		@SuppressWarnings("unused")
		IHM window=new IHMImpl("Smart Editor",new EngineImpl());
	}
}
